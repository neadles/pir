# pir

## Description
Simple PCB design allow a Panasonic EKMC1601111, AMN31112j or similar to be used on 24V. These signals can be easily used on a PLC for home / building automation.

## Housing
The housing is made to contain the pcb and allow for easy installation into a ceiling/wall with minimalistic presence.
The parts are designed to be printed in ASA for longevity with a layer height of 0.15mm so there is enough detail in the thread. 

The endcap has a hole of 4.6mm, designed to be used with PVC 3x0.25 cable which has an outer diameter of 4.5mm.

See housing/housing.3mf for print settings.

## Installation
<img src="images/steps.png" alt="3D model of the PCB">

## Tools
* EDA: KiCad v6
* CAD: Siemens NX
* Slicer: PrusaSlicer

## Models
### PCB
<img src="images/kicad_C2jwG3iQOc.png" alt="3D model of the PCB" width="400">

### Housing
<img src="images/pir_assembly.png" alt="3D model of the housing" width="400">
